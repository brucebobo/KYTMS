package com.kytms.rule.core;

import java.io.Serializable;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * 规则模型
 *
 * @author 臧英明
 * @create 2018-05-07
 */
public class RuleModel   implements Serializable{
    private String name;//名称
    private double version;//版本
    private Object code;//代码
}
