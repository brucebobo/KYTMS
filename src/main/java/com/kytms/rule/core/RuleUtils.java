package com.kytms.rule.core;

import com.kytms.rule.core.exception.RuleException;
import org.codehaus.groovy.tools.GroovyClass;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * 规则工具
 *
 * @author 臧英明
 * @create 2018-05-07
 */
public abstract class RuleUtils {
    private static final CacheRule cache = GroovyRule.getInstance();
    /**
     * 获取一个新的规则模型
     * @return
     */
    public static RuleModel getNewRuleModel(){
        return  new RuleModel();
    }
    public static GroovyClass toCompile(String str) throws RuleException {
        return (GroovyClass) cache.toCompile(str);
    }

    public static Object executeRule(RuleModel rullModel)throws RuleException {
        return cache.executeRule(rullModel);
    }

    public static Object executeRule(String id) throws RuleException {
        return cache.executeRule(id);
    }

    public static boolean isExist(String id) {
        return cache.isExist(id);
    }

    public static void saveRule(String id, RuleModel ruleModel) {
        cache.saveRule(id,ruleModel);
    }

    public static RuleModel getRule(String id) {
        return cache.getRule(id);
    }
}
